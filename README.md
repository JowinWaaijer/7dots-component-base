# README #

### What is this repository for? ###

This repository is used for storing all 7dots custom lightning components so they can be reused.

### Components ###

* Salesforce lightning lookup component


### Salesforce lightning lookup component ###

**author:** Jowin Waaijer

**description:** 

> Right now there is no OOTB (Out of the box) lightning component for Object Lookups.
> This component leverages the *Salesforce lightning design System* and the strength of lightning components to create a Lightning look and feel for our beloved lookup components

__Parameters__

| Name        | Type        | Required  | Default | Description |
| ------------|-------------|-----|---------| ----------- |
| sObjectName | String | Yes | - | The API Name of the Object you want to Query |
| labelField  | String | Yes | - | The value of the Field that will be shown to the end user |
| valueField  | String | Yes | - | The value of the Field that will be used for unique identification (usually ID)|
|   icon    | String | Yes  | - | Icon to be shown next next to the search results |
| label      | String      |  No | - | Label above the text field |
| placeholder | String     |    No | Search... | Label shown inside the text fields |
| additionalFields | String[] | No | - | Additional fields to be returned within the search query |
| SOSLsearchFields | String | No | ALL | Defines which fields to search for results (SOSL) Possible values: ALL or NAME |
| singleSelect | Boolean | No | true | When true selecting a value will block the search and show the selected result. When false the search result will be cleared and you will be able to select a next value |


__Events__

| Name        | Type        | Description |
| ------------|-------------|-------------|
| valueSelected | COE_GEN_LE_TypeAhead_ValueSelected | returns the selected object and object type | 
| onClear | COE_GEN_LE_TypeAhead_ValueSelected | returns the removed object and object type|

### Screenshots ###
![Schermafbeelding 2017-04-21 om 15.39.34.png](https://bitbucket.org/repo/jggKdqM/images/2199213941-Schermafbeelding%202017-04-21%20om%2015.39.34.png)

![Schermafbeelding 2017-04-21 om 15.39.41.png](https://bitbucket.org/repo/jggKdqM/images/508685413-Schermafbeelding%202017-04-21%20om%2015.39.41.png)

__Example__
```
#!html

<c:COE_GEN_LC_LookupTypeahead valueSelected="{!c.urgencySelected}" sObjectName="BMCServiceDesk__Urgency__c" showNewListItem="false" icon="standard:contact" labelField="Name" SOSLsearchFields="NAME" valueField="Id" label="Urgency" />
```