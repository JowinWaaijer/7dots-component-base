/**
 * Created by JowinWaaijer on 05-04-17.
 */
({
    doInit : function(component, event, helper) {
        helper.setSObjectPluralLabel(component, component.get('v.sObjectName'));

        var showSearchListItem = component.get('v.showSearchListItem');
        if(!showSearchListItem)
	        $A.util.addClass(component.find("SearchListItem"), 'slds-hide');

        var showNewListItem = component.get('v.showNewListItem');
        if(!showNewListItem)
	        $A.util.addClass(component.find("NewListItem"), 'slds-hide');
    },
	keyup : function(component, event, helper) {
	    if(component.get('{!v.singleSelect}') && component.get('{!v.hasValue}'))
	    	return;

        var searchInput = component.find("searchInput").get("v.value");
        var sObjectName = component.get('v.sObjectName');
        var labelField = component.get('v.labelField');
        var valueField = component.get('v.valueField');
        var SOSLsearchFields = component.get('v.SOSLsearchFields');
        var additionalFields = component.get('v.additionalFields');

        component.set('v.searchInput', searchInput);

        if(searchInput.length >= 3) {
        	helper.search(component, searchInput, sObjectName, labelField, valueField, SOSLsearchFields, additionalFields);
        } else {
            helper.closeLookup(component);
        }
	},
	itemSelected: function(component, event, helper)    {
        var valueField = component.get('{!v.valueField}');
        var labelField = component.get('{!v.labelField}');
        var resultList = component.get('{!v.resultList}');

        for(var prop in resultList[0])   {
            if(prop.toLowerCase() === labelField)   {
                  labelField = prop;
            }
            if(prop.toLowerCase() === valueField)   {
                valueField = prop;
            }
        }

        var selected = resultList.filter(function(result) {

            if(result[valueField] === event.currentTarget.dataset.objectid) {
                return true;
            }
            return false;
        });

		if(component.get("{!v.singleSelect}"))	{
		    component.set('{!v.valueLabel}', selected[0][labelField]);
  		}	else {
			component.find('searchInput').set('{!v.value}', '');
			component.find('searchInput').getElement().focus();
    	}

        component.set('{!v.value}', selected);
        component.set('{!v.hasValue}', true);
		helper.notifySubscribers(component, 'valueSelected');
        helper.closeLookup(component);

    },
    clearValue: function(component, event, helper)	{ // clears our value when singleSelect = true
        component.set('{!v.hasValue}', false);
        component.set('{!v.value}', {});
        component.set('{!v.valueLabel}', '');
        helper.notifySubscribers(component, 'onClear');
        component.focus();
    }
})