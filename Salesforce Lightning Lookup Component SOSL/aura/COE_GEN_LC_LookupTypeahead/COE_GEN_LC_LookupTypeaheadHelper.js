/**
 * Created by JowinWaaijer on 05-04-17.
 */
({
	search : function(component, searchInput, sObjectName, labelField, valueField, SOSLsearchFields, additionalFields) {
        this.toggleSpinner(component);

		var action = component.get("c.getSObjects");
        action.setParams({
            "searchInput": searchInput,
            "sObjectName": sObjectName,
            "labelField": labelField,
            "valueField": valueField,
            "SOSLsearchFields": SOSLsearchFields
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set('v.resultList', response.getReturnValue());
                var resultWrappers = [];

                if(response.getReturnValue().length === 0)  {
                    component.set('v.resultWrapper', resultWrappers);
                    this.openLookup(component);
                    return;
                }


                for(var result in response.getReturnValue()) {
                    for(var prop in response.getReturnValue()[result])   {
                       if(prop.toLowerCase() === labelField)   {
                           labelField = prop;
                           break;
                       }
                     }
                    resultWrappers.push({
                        Id: response.getReturnValue()[result].Id,
                        Name: response.getReturnValue()[result][labelField]
                    });
                }
                console.log(response.getReturnValue());
                console.log(resultWrappers);
                component.set('v.resultWrapper', resultWrappers);

                this.openLookup(component);
            }   else {
                 component.set('v.resultWrapper', []);
                 this.openLookup(component);
                 return;
            }
            this.toggleSpinner(component);
        });
        $A.enqueueAction(action);
	},
    setSObjectPluralLabel : function(component, sObjectName) {
        var action = component.get("c.getPluralSObjectName");
        action.setParams({
            "sObjectName": sObjectName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set('v.sObjectPluralLabel', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    openLookup : function(component) {
        var lookupDiv = component.find("searchLookup");
        if(!$A.util.hasClass(lookupDiv, 'slds-is-open'))
	    	$A.util.addClass(lookupDiv, 'slds-is-open');
    },
    closeLookup : function(component) {
        var lookupDiv = component.find("searchLookup");
        if($A.util.hasClass(lookupDiv, 'slds-is-open'))
	        $A.util.removeClass(lookupDiv, 'slds-is-open');
    },
    toggleSpinner : function(component) {
        var spinner = component.find("spinner");
        if($A.util.hasClass(spinner, 'slds-hide'))
	        $A.util.removeClass(spinner, 'slds-hide');
        else
            $A.util.addClass(spinner, 'slds-hide');
    },
    notifySubscribers    : function(component, eventType)   {
        var selectedObject = component.get('{!v.value}');
        if(Array.isArray(selectedObject))
            selectedObject = selectedObject[0];

        var event = component.getEvent(eventType);
        console.log('Firing event: ' + eventType, event);
        console.log(selectedObject);
        event.setParams({'selectedObject' : selectedObject, 'sobjectType': component.get('{!v.sObjectName}')}).fire();
    }
})