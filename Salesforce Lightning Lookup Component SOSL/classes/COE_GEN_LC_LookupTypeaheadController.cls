/**
 * Created by JowinWaaijer on 05-04-17.
 */

global with sharing class COE_GEN_LC_LookupTypeaheadController {
	@AuraEnabled
	global static List<SObject> getSObjects(String searchInput, String sObjectName, String labelField, String valueField, String SOSLsearchFields, List<String> additionalFields) {
		String addFields = '';

		for(String s : additionalFields)    {
			addFields += ',';
			addFields += s;
		}

		String searchQuery = 'FIND \''+searchInput+'*\' IN '+SOSLsearchFields+' FIELDS RETURNING '+sObjectName+'('+valueField+','+labelField + addFields + ')';
		System.debug(searchQuery);
		List<List<SObject>>searchList = search.query(searchquery);

		for(List<SObject> sObjectList : searchList) {
			if(String.valueOf(sObjectList.getSObjectType()).equalsIgnoreCase(sObjectName)) {
				return sObjectList;
			}
		}
		return new List<SObject>();
	}

	@AuraEnabled
	global static String getPluralSObjectName(String sObjectName) {
		try {
			SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
			Schema.DescribeSobjectResult sObjectDescribe = sObjectType.getDescribe();
			return sObjectDescribe.LabelPlural;
		} catch (NullPointerException e) {
			return 'Correct sObjectName? No Plural name found';
		}
	}
}